## Usage statistics

This file is a launcher usage statistics archive. You can see here which journey we made to make the launcher looks like it looks

Our current statistics you can find in [readme](../../README.md)

### 2.2.0 — 29 total

<img src="../pics/stats/2.2.0.png">

### 2.3.0 — 99 total

<img src="../pics/stats/2.3.0.png">

> You can suggest colors for your countries